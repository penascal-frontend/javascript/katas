# Katas Javascript

[Katas de Javascript de CodeWars](https://www.codewars.com/kata/search/?q=&beta=false&order_by=rank_id%20asc) resueltas.

Los resultados de los tests se muestran en la consola de las herramientas de desarrollo del navegador.

## Solved katas
1. [Collinearity](https://www.codewars.com/kata/65ba420888906c1f86e1e680). Comprobar si dos vectores son colineares.

1. [Neutralisation](https://www.codewars.com/kata/65128732b5aff40032a3d8f0). Comprobar caracteres comunes de dos textos.

1. [A Letter's Best Friend](https://www.codewars.com/kata/64fc03a318692c1333ebc04c). Comprobar si una letra está inmendiatemante después de otra, en un texto dado. 

1. [Flick Switch](https://www.codewars.com/kata/64fbfe2618692c2018ebbddb). Partiendo de un array de textos, devuelve un array de valores booleanos del mismo tamaño. éste se rellenará con el valor inicial `true`, pero cada vez que se encuentre con un texto `flick`, el valor cambiara de signo. Esto es: si está rellenando con `true` y cambia de signo, continuará rellenando con `false`. 
