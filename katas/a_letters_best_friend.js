export function aLettersBestFriend(text, letter, friend) {
    if (text.slice(-1) == letter ||
        !text.includes(letter) ||
        !text.includes(friend)
    ) return false

    let pos = text.indexOf(letter)
    while (pos >= 0 && pos + 1 < text.length) {
        if (text[pos+1] != friend) return false
        pos = text.indexOf(letter, pos + 1)
    }
    return true
}


if (window.namespaces.testing.enabled) {
    import('/lib/testing.js')
      .then(testing => testing.launch((new Error).fileName))
      .catch(err => console.log(err))
}
