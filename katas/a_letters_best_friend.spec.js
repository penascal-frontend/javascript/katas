import { it, assert } from '/lib/testing.js'
import { aLettersBestFriend } from './a_letters_best_friend.js'

const testdefs = [
  {values: ["he headed to the store", "h", "e"], expected: true},
  {values: ['abcdee', 'e', 'e'], expected: false},
  {values: ["i found an ounce with my hound", "o", "u"], expected: true},
  {values: ["we found your dynamite", "d", "y"], expected: false},
] 

const make_test = (values, expected) =>
  it(
    `In the text '${values[0]}', '${values[1]}' is ${expected ? '': 'NOT '}best friend of '${values[2]}'`, 
    () => assert(aLettersBestFriend(...values) === expected)
  )

export const tests = () => testdefs.forEach(x => make_test(x.values, x.expected))
