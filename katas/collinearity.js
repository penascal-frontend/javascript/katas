export function collinearity(x1, y1, x2, y2) {
    if (!x1 && !y1 || !x2 && !y2 ) return true
    
    const angle1 = !y1 ? 0 : !x1 ? 90 : y1 / x1
    const angle2 = !y2 ? 0 : !x2 ? 90 : y2 / x2
    return angle1 === angle2
}

if (window.namespaces.testing.enabled) {
    import('/lib/testing.js')
      .then(testing => testing.launch((new Error).fileName))
      .catch(err => console.log(err))
}
