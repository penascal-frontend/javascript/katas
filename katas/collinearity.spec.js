import { it, assert } from '/lib/testing.js'
import { collinearity } from './collinearity.js'

const testdefs = [
  {values: [1,1,1,1], expected: true},
  {values: [1,2,2,4], expected: true},
  {values: [1,1,6,1], expected: false},
  {values: [1,2,-1,-2], expected: true},
  {values: [1,2,1,-2], expected: false},
  {values: [4,0,11,0], expected: true},
  {values: [0,1,6,0], expected: false},
  {values: [4,4,0,4], expected: false},
  {values: [0,0,0,0], expected: true},
  {values: [0,0,1,0], expected: true},
  {values: [5,7,0,0], expected: true},
] 

const make_test = (values, expected) =>
  it(
    `Lines (${values[0]},${values[1]}) and (${values[2]},${values[3]}) are ${expected ? '': 'NOT '}collinears`, 
    () => assert(collinearity(...values) === expected)
  )

export const tests = () => testdefs.forEach(x => make_test(x.values, x.expected))
