export function flick_witch(orig) {
    let current_value = true
    return orig.map(x => {
        if (x === 'flick') current_value = !current_value 
        return current_value
    })
}

if (window.namespaces.testing.enabled) {
    import('/lib/testing.js')
      .then(testing => testing.launch((new Error).fileName))
      .catch(err => console.log(err))
}
