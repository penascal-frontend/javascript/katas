import { it, assert } from '/lib/testing.js'
import { flick_witch } from './flick_witch.js'

const testdefs = [
  {values: ['codewars', 'flick', 'code', 'wars'] , expected: [true, false, false, false]},
  {values: ['flick', 'chocolate', 'adventure', 'sunshine'], expected: [false, false, false, false]},
  {values: ['bicycle', 'jarmony', 'flick', 'sheep', 'flick'], expected: [true, true, false, false, true]},
] 

const make_test = (values, expected) =>
  it(
    `values ${values} have to return ${expected}`, 
    () => assert(flick_witch(values).every((x, idx) => x === expected[idx]))
  )

export const tests = () => testdefs.forEach(x => make_test(x.values, x.expected))
