export function neutralisation(text1, text2) {
    if (text1.length != text2.length)
        throw 'Passed texts have not the same length'
   
    if (text1 === text2) return text1
    
    const res = []
    for (let idx = 0; idx < text1.length; idx++)
        res.push(text1[idx] == text2[idx] ? text1[idx] : '0')

    return res.join('')
}

if (window.namespaces.testing.enabled) {
    import('/lib/testing.js')
      .then(testing => testing.launch((new Error).fileName))
      .catch(err => console.log(err))
}
