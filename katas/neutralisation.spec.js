import { it, assert } from '/lib/testing.js'
import { neutralisation } from './neutralisation.js'

const testdefs = [
  {values: ["--++--", "++--++"], expected: '000000'},
  {values: ["-+-+-+", "-+-+-+"], expected: "-+-+-+"},
  {values: ["-++-", "-+-+"], expected: "-+00"},
] 


const make_test = (values, expected) =>
  it(
    `The mask of texts "${values[0]}" and "${values[1]}" is "${expected}"`, 
    () => assert(neutralisation(...values) === expected)
  )

export const tests = () => testdefs.forEach(x => make_test(x.values, x.expected))
